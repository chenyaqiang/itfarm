package com.tc.itfarm.api.enums;

/**
 * Created by wangdongdong on 2016/9/8.
 */
public enum LogType {

    LOGIN(1, "登陆"),
    ADD(2, "新增"),
    UPDATE(3, "修改"),
    DELETE(4, "删除");

    private Integer code;

    private String name;

    LogType(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static LogType valueOfCode(Integer code) {
        for (LogType e : LogType.values()) {
            if (e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }
}
