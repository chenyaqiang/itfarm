package com.tc.itfarm.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.dao.FavoriteDao;
import com.tc.itfarm.model.Favorite;
import com.tc.itfarm.model.FavoriteCriteria;
import com.tc.itfarm.service.FavoriteService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2016/9/5.
 */
@Service
public class FavoriteServiceImpl extends BaseServiceImpl<Favorite> implements FavoriteService {

    @Resource
    private FavoriteDao favoriteDao;

    @Override
    protected SingleTableDao getSingleDao() {
        return favoriteDao;
    }

    @Override
    public List<Integer> selectArticleIds(Integer userId) {
        Assert.notNull(userId);
        FavoriteCriteria criteria = new FavoriteCriteria();
        criteria.or().andUserIdEqualTo(userId);
        List<Favorite> favorites = favoriteDao.selectByCriteria(criteria);
        Set<Integer> ids = Sets.newHashSet();
        for (Favorite f : favorites) {
            ids.add(f.getArticleId());
        }
        return Lists.newArrayList(ids);
    }
}
