package com.tc.itfarm.web.biz;

import com.google.common.collect.Lists;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.model.*;
import com.tc.itfarm.service.*;
import com.tc.itfarm.web.vo.ArticleVO;
import com.tc.itfarm.web.vo.MenuVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ArticleBiz {
	
	@Resource
	private UserService userService;
	@Resource
	private CategoryService categoryService;
	@Resource
	private ArticleService articleService;
	@Resource
	private MenuService menuService;
	@Resource
	private MenuArticleService menuArticleService;
	@Resource
	private CommentService commentService;

	/**
	 * 获取文章VO
	 * @param articles
	 * @return
	 */
	public List<ArticleVO> getArticleVOs (List<Article> articles) {
		List<ArticleVO> articleVOs = Lists.newArrayList();
		for (Article article : articles) {
			ArticleVO vo = new ArticleVO();
			User user = userService.select(article.getAuthorId());
			Category category = categoryService.select(article.getTypeId());
			vo.setArticle(article);
			vo.setAuthorName(user.getNickname());
			vo.setCategoryName(category.getName());
			vo.setLastDate(DateUtils.dateToChineseString(article.getModifyTime()));
			vo.setCommentCount(commentService.countByArticle(article.getRecordId()));
			articleVOs.add(vo);
		}
		return articleVOs;
	}

	/**
	 * 获取单个
	 * @param recordId
	 * @return
	 */
	public ArticleVO getArticleVOById(Integer recordId) {
		Article article = articleService.select(recordId);
		article.setPageView(article.getPageView() == null ? 1 : (article.getPageView() + 1));
		articleService.updatePageView(article.getRecordId(), article.getPageView() == null ? 1 : (article.getPageView() + 1));
		ArticleVO vo = new ArticleVO();
		// 查询作者
		User user = userService.select(article.getAuthorId());
		// 分类
		vo.setCategoryName("未分类");
		if (article.getTypeId() != -1) {
			Category category = categoryService.select(article.getTypeId());
			vo.setCategoryName(category.getName());
		}
		// 查询上条和下条
		vo.setLast(articleService.selectLast(article.getTitle()));
		vo.setNext(articleService.selectNext(article.getTitle()));
		vo.setArticle(article);
		vo.setAuthorName(user.getNickname());
		vo.setLastDate(DateUtils.dateToChineseString(article.getModifyTime()));
		vo.setCommentCount(commentService.countByArticle(article.getRecordId()));
		return vo;
	}

	/**
	 * 生成保存图片名
	 * @param sourceName
	 * @return
	 */
	public String getImgFileName (String sourceName) {
		StringBuilder sb = new StringBuilder();
		sb.append(System.currentTimeMillis()).append((int)Math.random() * 100);
		sourceName = sourceName.substring(sourceName.lastIndexOf(".") + 1);
		return sb.append(".").append(sourceName).toString();
	}

	/**
	 * 获取菜单VO
	 * @return
	 */
	public List<MenuVO> getMenuVOs() {
		List<MenuVO> menuVOList = Lists.newArrayList();
		List<Menu> menus = menuService.selectAll();
		for (Menu m : menus) {
			MenuVO vo = new MenuVO();
			List<Integer> ids = menuArticleService.selectArticleByMenuId(m.getRecordId(), null);
			vo.setCount(ids.size());
			if (ids.size() > 4) {
				ids = ids.subList(0, 4);
			}
			List<Article> articles = articleService.selectByIds(ids);
			vo.setMenu(m);
			vo.setArticles(this.getArticleVOs(articles));
			menuVOList.add(vo);
		}
		return menuVOList;
	}

	/**
	 * 获取菜单VO
	 * @return
	 */
	public MenuVO getMenuVO(Integer menuId, Page page) {
		Menu menu = menuService.select(menuId);
		List<Integer> ids = menuArticleService.selectArticleByMenuId(menuId, page);
		List<Article> articles = articleService.selectByIds(ids);
		MenuVO vo = new MenuVO();
		vo.setMenu(menu);
		vo.setArticles(this.getArticleVOs(articles));
		return vo;
	}

	
}
