<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<%--右侧竖栏--%>
<div id="sidebar">
    <%--四张图片--%>
    <div class="ads box">

        <ul>
            <li>
                <a href="#?ref=smuliii"><img width="125" height="125" src="http://envato.s3.amazonaws.com/referrer_adverts/tf_125x125_v5.gif" alt="Themeforest"></a>
            </li>
            <li class="even">
                <a href="http://graphicriver.net/?ref=smuliii"><img width="125" height="125" src="http://envato.s3.amazonaws.com/referrer_adverts/gr_125x125_v4.gif" alt="Graphicriver"></a>
            </li>
            <li>
                <a href="http://activeden.net/?ref=smuliii"><img width="125" height="125" src="http://envato.s3.amazonaws.com/referrer_adverts/ad_125x125_v4.gif" alt="Activeden"></a>
            </li>
            <li class="even">
                <a href="http://codecanyon.net/?ref=smuliii"><img width="125" height="125" src="http://envato.s3.amazonaws.com/referrer_adverts/cc_125x125_v1.gif" alt="CodeCanyon"></a>
            </li>
        </ul>

        <p class="align-center"><a href="#">Advertise With Us</a></p>

    </div><!-- end .ads -->
    <%--最新显示--%>
    <div id="recent-tabs" class="box">

        <div class="box-header">

            <ul class="nav">
                <li><a class="current" href="#recent-tabs-posts">最新发表</a></li>
                <li><a href="#recent-tabs-comments">最新评论</a></li>
            </ul>

        </div><!-- end .box-header -->

        <div class="list-wrap">

            <ul id="recent-tabs-posts">
                <c:forEach items="${newArticles }" var="item" varStatus="vs">
                    <li>
                        <a href="${ctx }/article/detail.do?id=${item.article.recordId}" class="title">
                            <img src="${config.adminUrl }titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/001.png'" width="40" height="40" alt="" />
                                ${item.article.title}...
                        </a>
                        <p class="meta">编辑于 <a href="#">${item.lastDate}</a> by <a href="#">${item.authorName}</a></p>
                    </li>
                </c:forEach>
            </ul><!-- end #recent-tabs-posts-->

            <ul id="recent-tabs-comments" class="hide">

                <li>
                    <a href="single-post.html" class="title">
                        <img src="http://1.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=40" width="40" height="40" alt="" />
                        Lorem ipsum dolor sit amet...
                    </a>
                    <p class="meta">Commented on <a href="#">March 29, 2010</a> by <a href="#">John Doe</a></p>
                </li>

                <li>
                    <a href="single-post.html" class="title">
                        <img src="http://1.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=40" width="40" height="40" alt="" />
                        Lorem Ipsum is simply dummy text...
                    </a>
                    <p class="meta">Commented on <a href="#">March 17, 2010</a> by <a href="#">John Doe</a></p>
                </li>

                <li>
                    <a href="single-post.html" class="title">
                        <img src="http://1.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=40" width="40" height="40" alt="" />
                        Lorem ipsum dolor sit amet...
                    </a>
                    <p class="meta">Commented on <a href="#">March 3, 2010</a> by <a href="#">John Doe</a></p>
                </li>

            </ul><!-- end #recent-tabs-comments -->

        </div><!-- end .list-wrap -->

    </div><!-- end #recent-tabs -->
    <%--用户--%>
    <div class="flickr-feed box">

        <div class="box-header">

            <h6 class="align-left"><img src="${ctx}/images/icon-flickr-feed.png" alt="icon-flickr-feed" class="flickr-icon" /> 最近登录</h6>

            <a href="#" class="align-right">Sign In</a>

        </div><!-- end .box-header -->

        <ul>
            <li>
                <a href="http://www.flickr.com/photos/kwl/2432868269/">
                    <img src="img/sample-images/75x75.jpg" width="75" height="75" alt="" class="flickr-image" />
                </a>
            </li>
            <li>
                <a href="http://www.flickr.com/photos/dexxus/4338578468/">
                    <img src="img/sample-images/75x75.jpg" width="75" height="75" alt="" class="flickr-image" />
                </a>
            </li>
            <li>
                <a href="http://www.flickr.com/photos/extranoise/158135547/">
                    <img src="img/sample-images/75x75.jpg" width="75" height="75" alt="" class="flickr-image" />
                </a>
            </li>
            <li>
                <a href="http://www.flickr.com/photos/18614695@N00/3472778601/">
                    <img src="img/sample-images/75x75.jpg" width="75" height="75" alt="" class="flickr-image" />
                </a>
            </li>
            <li>
                <a href="http://www.flickr.com/photos/notsogoodphotography/466107969/">
                    <img src="img/sample-images/75x75.jpg" width="75" height="75" alt="" class="flickr-image" />
                </a>
            </li>
            <li>
                <a href="http://www.flickr.com/photos/brunociampi/2487769238/">
                    <img src="img/sample-images/75x75.jpg" width="75" height="75" alt="" class="flickr-image" />
                </a>
            </li>
        </ul>

    </div><!-- end .flickr-feed -->

    <div class="tags box">

        <div class="box-header">

            <h6>热门标签</h6>

        </div><!-- end .box-header -->

        <ul>
            <li><a href="#">New York City</a></li>
            <li><a href="#">Australia</a></li>
            <li><a href="#">Woods</a></li>
            <li><a href="#">Beach</a></li>
            <li><a href="#">Brooklyn</a></li>
            <li><a href="#">Bridge</a></li>
            <li><a href="#">Texas</a></li>
            <li><a href="#">2010</a></li>
            <li><a href="#">Palmtree</a></li>
            <li><a href="#">Miami</a></li>
            <li><a href="#">Museum</a></li>
            <li><a href="#">Native</a></li>
            <li><a href="#">American</a></li>
            <li><a href="#">Trips</a></li>
            <li><a href="#">Private</a></li>
            <li><a href="#">Family</a></li>
            <li><a href="#">Photo</a></li>
        </ul>

    </div><!-- end .tags -->
    <%--广告图片--%>
    <img src="${ctx}/images/ad.gif" alt="ad" class="ad-bar" />
    <a href="#?ref=smuliii"><img src="http://envato.s3.amazonaws.com/referrer_adverts/tf_300x250_v5.gif" alt="tf_300x250_v5" class="ad" /></a>

</div><!-- end #sidebar -->
</body>
</html>